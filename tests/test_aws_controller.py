import unittest
from unittest.mock import MagicMock, patch
import pytest
from app.aws_controller import AWSController


class TestAWSController(unittest.TestCase):
    def setUp(self):
        self.controller = AWSController()
        self.controller.dynamo_client = MagicMock()

        self.file = MagicMock()
        self.file.filename = 'filename'
        self.file.stream.read.return_value = b'birds are good'

        self.text = "chickens pigeon's chickens chicken's chickens 123 pigeons birds"
        self.freqs = [
            ('chickens', 4),
            ('pigeons', 2),
            ('birds', 1),
        ]

    @patch('app.aws_controller.AWSController.post_file')
    @patch('app.aws_controller.AWSController.transform_freq_for_dynamo')
    @patch('app.aws_controller.AWSController.count_words')
    @patch('werkzeug.utils.secure_filename')
    def test_process_file(self, mock_filename, mock_count_words, mock_transform, mock_post):
        mock_freqs = MagicMock()
        mock_count_words.return_value = mock_freqs

        self.controller.process_file(self.file)

        mock_count_words.assert_called_with('birds are good', False)
        mock_transform.assert_called_with(mock_freqs)
        mock_post.assert_called_once()

    def test_count_words(self):
        assert self.controller.count_words(self.text) == self.freqs

    @patch('app.aws_controller.AWSController.get_stop_words')
    def test_count_words_ignore_stop_words(self, mock_stop_words):
        mock_stop_words.return_value = ['pigeons']
        expected = [
            ('chickens', 4),
            ('birds', 1),
        ]
        assert self.controller.count_words(self.text, True) == expected

    def test_transform_freq_for_dynamo(self):
        expected = [
            {'M': {'chickens': {'N': '4'}}},
            {'M': {'pigeons': {'N': '2'}}},
            {'M': {'birds': {'N': '1'}}},
        ]
        assert self.controller.transform_freq_for_dynamo(self.freqs) == expected

    def test_post_file(self):
        self.controller.post_file(self.file.filename, MagicMock, MagicMock)
        self.controller.dynamo_client.put_item.assert_called_once()

    def test_get_stop_words(self):
        self.controller.dynamo_client.get_item.return_value = {
            'Item': {'text': {'L': [
                {'S': 'chicken'},
                {'S': 'cube'},
                {'S': 'buttercup'},
            ]}}
        }
        expected = ['BUTTERCUP', 'CHICKEN', 'CUBE']
        assert self.controller.get_stop_words() == expected

    def test_set_stop_words(self):
        words = "chicken\r\ncube\r\nbuttercup"
        expected = [
            {'S': 'BUTTERCUP'},
            {'S': 'CHICKEN'},
            {'S': 'CUBE'},
        ]

        self.controller.set_stop_words(words)
        self.controller.dynamo_client.update_item.assert_called_with(
            TableName=self.controller.DYNAMO_TABLE,
            Key=self.controller.STOP_WORDS_KEY,
            AttributeUpdates={
                'text': {
                    'Value': {
                        'L': expected
                    },
                    'Action': 'PUT'
                }

            }
        )

