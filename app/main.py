import re
from flask import Flask, request, jsonify, render_template
from app.aws_controller import AWSController

app = Flask(__name__)
aws = AWSController()

@app.route('/', methods=['GET', 'POST'])
def index():
    error = None
    if request.method == 'POST':
        if 'file' not in request.files:
            error = 'No file selected'
        file = request.files['file']
        if file.filename == '':
            error = 'No file selected'
        elif file.filename.rsplit('.', 1)[1].lower() == 'txt':
            ignore_stop_words = bool(request.form.get('stop_words'))
            stem_suffixes = bool(request.form.get('stem_suffixes'))
            aws.process_file(file, ignore_stop_words, stem_suffixes)
        else:
            error = 'Unrecognized file format'

    files = aws.get_files()
    return render_template('main.html', files=files, error=error)

@app.route('/admin', methods=['GET', 'POST'])
def admin():
    error = None
    updated_stop_words = False
    updated_suffixes = False
    if request.method == 'POST':
        if request.form.get('stopwords'):
            words = request.form.get('stopwords')
            aws.set_stop_words(words)
            updated_stop_words = True
        elif request.form.get('suffixes'):
            words = request.form.get('suffixes')
            try:
                aws.set_suffixes(words)
                updated_suffixes = True
            except IndexError:
                error = 'Error parsing suffixes'

    stop_words = '\n'.join(aws.get_stop_words())
    suffixes = '\n'.join(aws.get_pretty_suffixes())
    return render_template(
        'admin.html',
        suffix_error=error,
        stop_words=stop_words,
        suffixes=suffixes,
        updated_stop_words=updated_stop_words,
        updated_suffixes=updated_suffixes
    )


if __name__ == '__main__':
    app.run()
