import itertools
from datetime import datetime
import os
import re
import boto3
from werkzeug.utils import secure_filename


class AWSController:
    DYNAMO_TABLE = os.environ.get('DYNAMO_TABLE') or 'farmers-fridge-interview-dmcginty'
    STOP_WORDS_KEY = {
        'index': {'S': 'STOPWORDS'},
        'created_at': {'S': 'null'}
    }
    SUFFIXES_KEY = {
        'index': {'S': 'SUFFIXES'},
        'created_at': {'S': 'null'}
    }
    DATE_FORMAT = "%Y.%m.%d.%H:%M:%S"

    def __init__(self):
        self.dynamo_client = boto3.client('dynamodb')

    def scan(self):
        return self.dynamo_client.scan(TableName=self.DYNAMO_TABLE)

    def process_file(self, file,
            ignore_stop_words=False,
            stem_suffixes=False):
        filename = secure_filename(file.filename)
        text = ''.join(chr(e) for e in file.stream.read())
        freqs = self.count_words(
            text,
            ignore_stop_words,
            stem_suffixes
        )
        freqs = self.transform_freq_for_dynamo(freqs)
        self.post_file(
            filename,
            text,
            freqs,
            ignore_stop_words,
            stem_suffixes)

    def attempt_to_stem(self, word, vocab, suffixes):
        for suffix, replace in suffixes:
            if not word.endswith(suffix):
                continue
            new_word = word[:-len(suffix)] + replace
            if new_word in vocab:
                word = new_word
                break
        return word

    def stem_suffixes(self, processed):
        suffixes = self.get_suffixes()
        vocab = set(processed)
        text = []
        for word in processed:
            text.append(self.attempt_to_stem(word, vocab, suffixes))
        return text

    def process_text_for_counting(self, text, stem_suffixes):
        processed = []
        regex = re.compile('[^A-Za-z]')
        for word in text.split():
            word = regex.sub('', word)
            if len(word) == 0:
                continue
            processed.append(word)
        if stem_suffixes:
            processed = self.stem_suffixes(processed)
        return processed

    def count_words(self, text, ignore_stop_words, stem_suffixes):
        stop_words = []
        if ignore_stop_words:
            stop_words = self.get_stop_words()

        frequencies = {}
        processed = self.process_text_for_counting(text, stem_suffixes)

        for word in processed:
            if word in stop_words:
                continue
            if word not in frequencies:
                frequencies[word] = 0
            frequencies[word] += 1
        freq_sorted = {
                k: v for k,v in sorted(
                    frequencies.items(),
                    key=lambda x: x[1],
                    reverse=True)
        }
        return list(itertools.islice(freq_sorted.items(), 25))

    def transform_freq_for_dynamo(self, freqs):
        freq_list = []
        for word, freq in freqs:
            freq_list.append({
                'M': {word: {'N': str(freq)}}
            })
        return freq_list

    def post_file(self, filename, text, freqs, ignore_stop_words, stem_suffixes):
        self.dynamo_client.put_item(
            TableName=self.DYNAMO_TABLE,
            Item={
                'index': {
                    'S': 'FILE'
                },
                'created_at': {
                    'S': datetime.now().strftime(self.DATE_FORMAT)
                },
                'filename': {
                    'S': filename
                },
                'text': {
                    'S': text
                },
                'frequencies': {
                    'L': freqs
                },
                'ignore_stop_words': {
                    'BOOL': ignore_stop_words
                },
                'stem_suffixes': {
                    'BOOL': stem_suffixes
                },
            })

    def get_files(self):
        response = self.dynamo_client.query(
            TableName=self.DYNAMO_TABLE,
            AttributesToGet=[
                'filename',
                'text',
                'frequencies',
                'ignore_stop_words',
                'stem_suffixes',
                'created_at',
            ],
            Limit=10,
            KeyConditions={
                'index': {
                    'AttributeValueList': [{'S': 'FILE'}],
                    'ComparisonOperator': 'EQ'
                }
            }
        )
        file_objs = response['Items']

        files = []
        for n, file in enumerate(file_objs):
            filename = file.get('filename', {'S':'[no title]'})['S']
            text = file.get('text', {'S':''})['S']
            created_at = file.get('created_at', {'S':''})['S']
            created_at = datetime.strptime(
                created_at,
                self.DATE_FORMAT
            )
            created_at = created_at.strftime("%d/%m/%Y %H:%M:%S")
            stop_words = file.get('ignore_stop_words', {'BOOL':False})['BOOL']
            stem_suffixes = file.get('stem_suffixes', {'BOOL': False})['BOOL']
            key = 'file{0}'.format(n)
            freq_objs = file.get('frequencies', {'L':[]})['L']
            freqs = []
            for item in freq_objs:
                word = list(item['M'].keys())[0]
                freq = item['M'][word]['N']
                freqs.append((word, freq))
            files.append({
                'filename': filename,
                'text': text,
                'created_at': created_at,
                'key': key,
                'frequencies': freqs,
                'ignore_stop_words': stop_words,
                'stem_suffixes': stem_suffixes,
            })

        return files[::-1]

    def get_stop_words(self):
        response = self.dynamo_client.get_item(
            TableName=self.DYNAMO_TABLE,
            Key=self.STOP_WORDS_KEY
        )
        words = response['Item']['text']['L']
        words = sorted([list(w.values())[0].upper() for w in words])
        return words

    def set_stop_words(self, stop_words):
        words = []
        regex = re.compile('[^A-Za-z]')
        stop_words = sorted(stop_words.split())
        for word in stop_words:
            word = regex.sub('', word)
            if len(word) == 0:
                continue
            words.append({'S': word.upper()})

        self.dynamo_client.update_item(
            TableName=self.DYNAMO_TABLE,
            Key=self.STOP_WORDS_KEY,
            AttributeUpdates={
                'text': {
                    'Value': {
                        'L': words
                    },
                    'Action': 'PUT'
                }
            }
        )

    def get_suffixes(self):
        response = self.dynamo_client.get_item(
            TableName=self.DYNAMO_TABLE,
            Key=self.SUFFIXES_KEY
        )
        items = response['Item']['text']['L']
        suffixes = []
        for item in items:
            suffix = list(item['M'].keys())[0]
            replace = item['M'][suffix]['S']
            suffixes.append([suffix, replace])
        return suffixes

    def get_pretty_suffixes(self):
        raw = self.get_suffixes()
        suffixes = []
        for suffix, replace in raw:
            pre_str = '"{0}" -> "{1}"'.format(suffix, replace)
            suffixes.append(pre_str)
        return sorted(suffixes)

    def set_suffixes(self, suffixes):
        regex = re.compile('[^A-Za-z]')
        translations = sorted(suffixes.split('\r\n'))
        suffixes = []
        for t in translations:
            if len(t) < 7:
                continue
            w = [regex.sub('', w).upper() for w in t.split(' -> ')]
            suffixes.append({
                'M': {w[0]: {'S': w[1]}}
            })

        self.dynamo_client.update_item(
            TableName=self.DYNAMO_TABLE,
            Key=self.SUFFIXES_KEY,
            AttributeUpdates={
                'text': {
                    'Value': {
                        'L': suffixes
                    },
                    'Action': 'PUT'
                }
            }
        )
