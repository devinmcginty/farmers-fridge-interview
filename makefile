run:
	FLASK_APP=app/main:app FLASK_ENV=development flask run

serve:
	gunicorn --chdir app main:app

setup: requirements.txt
	pip install -r requirements.txt

test:
	pytest tests/
