# Farmers Fridge Code Exercise
This is a coding exercise done for a technical interview with Farmers Fridge on 21 July 2020. This project took around 10-12 hours to complete over 2 days. This does not include some periods of time that were not directly spent developing (writing this document, configuring AWS, writing some of the styling, and work on an additional feature that I wound up not implementing). Overall, I'd estimate that the total time spent was 14 hours.

## Live Version
http://3.23.3.70/
I can't guarantee that this will remain up after 21 July 2020.

## Setup
Most of the setup has been built into the provided makefile.
```
$ make setup
$ make test
$ make
```
Setup requires configuration of the AWS CLI using valid credentials. The development environment uses the production database, so in order to run this you will need valid credentials for the production database. If you wish to use a database other than the default you can pass it as an environment variable `DYNAMO_TABLE`:
```
$ export DYNAMO_TABLE=table-name make run
```

## Why Gitlab?
My original motivation for using Gitlab was to take advantage of their auto-devops service. However, Python is not natively supported, and the Python Heroku buildpack that I tried to use crashes during setup. Therefore, all pipelines fail due to the broken buildpack. Tests should be run in the development environment before a pull request is created.

## Technology and techniques
[![forthebadge made-with-python](http://ForTheBadge.com/images/badges/made-with-python.svg)](https://www.python.org/)

The application was built using Python3, [Flask](https://flask.palletsprojects.com/en/1.1.x/), [Boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html) (AWS's Python SDK), and pytest.
The backend architecture was built on AWS using EC2 and DynamoDB, with [Gunicorn](https://gunicorn.org/) run by `systemctl`.
The front end uses [Bootstrap](https://getbootstrap.com/), [jQuery](https://jquery.com/), and [VanillaJS](http://vanilla-js.com/).
The background image was generated using [PatternCooler](https://www.patterncooler.com/).

## Suffix stemming
This is the algorithm that I'm using for suffix stemming:
1. Strip punctuation from all words in the text.
2. Create a set of all words in the text to create a vocabulary of unique words.
3. Iterate over every word
4. If the word ends with one of the suffixes:
	1. Strip the suffix from the word and replace with the replacement substring (if provided)
	2. Check to see if this new word is in the original text
	3. If so, replace the word with the new word

The complexity of this method would be *O(n &times; m)* where *n* is the length of the text and *m* is the number of suffixes. For most practical uses the complexity would be *O(n)*.

This method does not account for recursive suffixes or transitive suffixes. For example, provided a text of `M MAAA MAAB MABA AAA` and generation rules:
> { AB &rightarrow; A }
> { A &rightarrow; "" }

This algorithm would not consider any of these words to have suffixes, even though they can be created by concatenating suffixes on the word `M`.


## Admin page
There is an administration page located at `/admin` for editing the stop words and suffixes. The stop words are alphabetical, one per line.
The suffixes use a replacement syntax of `"suffix" -> "replacement"`. Suffixes that are removed are replaced with an empty string. For example:
> { L &rightarrow; "" }
> { EZL &rightarrow; R }

would be entered into the suffix editor as
```
"L" -> ""
"EZL" -> "R"
```
All stop words and suffixes are alphabetized before being saved.

## Notes and additional work
* There is no authentication for the `/admin` page.
* Tests are needed for some parts of the codebase
* Get the auto-Devops buildpack to work or find a new buildpack
* Create Ansible playbook for deployment from Gitlab.
* When running the application with Gunicorn the following line in `main.py` must be changed from
  `from app.aws_controller import AWSController`
  to
  `from aws_controller import AWSController`
If an Ansible playbook was to be created this should be handled automatically before deployment to production.
* Code readability could be improved (commenting, linting, docstrings).
* The `POST` endpoints currently do very little input validation and could possible expose an Exception to the user.
* HTTPS is currently not supported.
* The front end is not tested. That being said, it's very barebones. (There's almost no JS.)
* The authentication on the EC2 instance is currently done using an AWS access key. A better pattern would be to peer the EC2 instance and the DynamoDB table in a VPC and authenticate using IAM.
* The only supported document type is `.txt`.
* A lot of the HTML and CSS could be improved. Currently the two pages share a CSS file, and there's overlap between the Jinja templates.
* I was going to make a button to delete files, but I didn't get around to it.
* Scalability concerns:
	* The Elastic IP currently points to a single t2.micro EC2 instance. A realistic use case should handle the compute resources using an autoscaling group.
	* The part of the application that handles word counting, stop words, and suffixes could slow down significantly by feeding the program a sufficiently large list of suffixes and lengthy text documents. This service could be moved to its own microservice, and/or could be written in a more performant language.
	* All data is currently housed in a single DynamoDB table for ease of development. In a larger service I would separate the data into tabular data and text documents. The tabular data would be put into a relational database, probably Aurora, and the text documents would be saved to an S3 bucket.
